from django.db import models
from on_demand.models import Profile, AuditModel,generate_uniq_id


class Vehicle(AuditModel):
    profile = models.ForeignKey(Profile, on_delete=models.DO_NOTHING)
    vehicle_no = models.CharField(max_length=30)
    is_insured = models.BooleanField(default=False)
    brand = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    seats = models.PositiveSmallIntegerField(default=2)

    def __str__(self):
        return self.vehicle_no


class InsureTrip(AuditModel):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.DO_NOTHING)
    travellers = models.ManyToManyField(Profile)
    starting_loc_lat = models.FloatField()
    starting_loc_lon = models.FloatField()
    destination_loc_lat = models.FloatField()
    destination_loc_lon = models.FloatField()
    departure_time = models.DateTimeField()
    arrival_time = models.DateTimeField()
    estimated_distance = models.FloatField()
    is_paid = models.BooleanField(default=False)
    is_claim_completed = models.BooleanField(default=False)
    split_payments = models.BooleanField(default=False)
    trip_info_ref = models.CharField(max_length=255, unique=True, default=generate_uniq_id)

    def __str__(self):
        return self.trip_info_ref


class TripPayment(AuditModel):
    trip_info = models.ForeignKey(InsureTrip, on_delete=models.DO_NOTHING)
    claim_amount = models.FloatField()
    payment_ref = models.CharField(max_length=255, unique=True, default=generate_uniq_id)

    def __str__(self):
        return self.trip_info
