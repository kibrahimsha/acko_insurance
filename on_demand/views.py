import profile

import django.http
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from on_demand.models import Profile, Assest, InsureAssest, Chat
import json
from django.db.models import Q
from datetime import datetime, timedelta


def user_register(request):
    if request.method == "POST":
        email = request.POST.get('email')
        name = request.POST.get('name')
        password = request.POST.get('password')
        user = User.objects.create(username=email, email=email, first_name=name)
        user.set_password(password)
        group = Group.objects.get(id=2)
        user.groups.add(group)
        user.save()
        Profile.objects.create(user=user)
        messages.add_message(request, messages.SUCCESS, 'Customer Registered Successfully.')
        return HttpResponseRedirect(reverse('user_login'))
    else:
        return render(request, 'signup.html', context={})


def user_login(request):
    """

    :param request:
    :return:
    """
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(username=email, password=password)
        if user is None:
            messages.add_message(request, messages.ERROR, 'The email address or password is incorrect.')
        else:
            login(request, user)
            return HttpResponseRedirect(reverse('user_profile'))
    return render(request, 'login.html', context={})


def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('user_login'))


def user_profile(request):
    if request.method == "POST":
        user = request.user
        profile = request.user.profile
        user.first_name = request.POST.get('name')
        profile.pan_card_no = request.POST.get('pan_card_no')
        profile.aadhaar_card_no = request.POST.get('aadhaar_card_no')
        profile.mobile_no = request.POST.get('mobile_no')
        user.save()
        profile.save()
        messages.add_message(request, messages.SUCCESS, 'Profile Updated Successfully.')
    return render(request, 'profile.html', context={'user': request.user})


def create_assest(request):
    if request.method == "POST":
        name = request.POST.get('name')
        invoice = request.FILES.get('invoice')
        image = request.FILES.get('image')
        brand = request.POST.get('brand')
        model = request.POST.get('model')
        purchased_at = request.POST.get('purchased_at')
        amount = request.POST.get('amount')
        serial_no = request.POST.get('serial_no')
        assest = Assest.objects.create(
            name=name,
            invoice=invoice,
            brand=brand,
            model=model,
            purchased_at=datetime.strptime(purchased_at, '%b %d, %Y'),
            amount=amount,
            serial_no=serial_no,
            status='P',
            profile=request.user.profile,
            image=image
        )
        messages.add_message(request, messages.SUCCESS, 'Assest Created Successfully.')
        return HttpResponseRedirect(reverse('assest_list')) # TODO: Assest List
    return render(request, 'add-asset.html', context={})


def assest_list(request):
    assests = Assest.objects.filter(profile=request.user.profile)
    return render(request, 'asset-list.html', context={'assests': assests})

def assest_detail(request, assest_ref):
    current_datetime = datetime.now()
    assest = Assest.objects.get(assest_ref=assest_ref)
    insured_assests = InsureAssest.objects.filter(assest=assest).last()
    context = {
        'assest': assest,
        'insured_assests': insured_assests,
    }
    return render(request, 'asset-detail.html', context=context)


def get_premiums(request):
    assest_ref = request.POST.get('assestId')
    start_time = datetime.strptime(request.POST.get('startTime'), '%b %d, %Y %I:%M %p')
    end_time = request.POST.get('endTime')
    diff = datetime.strptime(end_time, '%b %d, %Y %I:%M %p') - start_time
    days, seconds = diff.days, diff.seconds
    hours = days * 24 + seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    assest = Assest.objects.get(assest_ref=assest_ref)
    premiums = []
    for i in range(0, 6):
        new_hours = hours + (i * 4)
        new_time = start_time + timedelta(hours=new_hours)
        premiums.append(
            {
                "hours": new_hours,
                "startTime": request.POST.get('startTime'),
                "endTime": new_time.strftime('%b %d, %Y %I:%M %p'),
                "amount": round(assest.amount * (hours + (i * 4)) * 0.000111, 2)
            }
        )
    return HttpResponse(json.dumps(premiums))


def create_insure_assest(request):
    if request.method == "POST":
        assest_ref = request.POST.get('assestId')
        start_time = request.POST.get('startTime')
        end_time = request.POST.get('endTime')
        premium = request.POST.get('premium')
        assest = Assest.objects.get(assest_ref=assest_ref)
        InsureAssest.objects.create(
            assest=assest,
            start_time=datetime.strptime(start_time, '%b %d, %Y %I:%M %p'),
            end_time=datetime.strptime(end_time, '%b %d, %Y %I:%M %p'),
            premium=premium,
            is_paid=True
        )
        data = {
            "message": "Assest -{0}({1}) Insured Successfully".format(assest.name, assest.assest_ref)
        }
        return HttpResponse(json.dumps(data), status=200)
    else:
        data = {
            "message": "Assest not Insured"
        }
        return HttpResponse(json.dumps(data), status=500)


def insure_assest_list(request):
    insure_assests = InsureAssest.objects.filter(assest__profile__user=request.user)
    return render(request, 'insure-asset-list.html', context={'insure_assests': insure_assests})


def chat_history(request):
    chats = Chat.objects.filter(Q(receiver=request.user) | Q(sender=request.user))
    return render(request, '', context={'chats': chats})


def create_chat(request):
    if request.method == "POST":
        receiver = request.POST.get('receiver_id')
        message = request.POST.get('message')
        insured_assest_id = request.POST.get('insured_assest_id')
        chat = Chat.objects.create(
            sender_id=request.user.id,
            receiver_id=receiver,
            message=message,
            subject='A',
            insured_assest_id=insured_assest_id,
        )
        data = {
            "message": "Chat Updated Successfully"
        }
        return HttpResponse(json.dumps(data), status=200)


def claim_insurance(request, assest_ref):
    if request.method == "GET":
        chats = Chat.objects.filter(insured_assest__insure_assest_ref=assest_ref)
        insured_asset = InsureAssest.objects.get(insure_assest_ref=assest_ref)
        receiver_id = insured_asset.assest.profile.user.id
        if request.user.groups.first().name == 'Admin':
            receiver_id = User.objects.get(username='admin@insurance.com').id
        context = {
            'insured_asset': insured_asset,
            'chats': chats,
            'receiver_id': receiver_id,
        }
        return render(request, 'claim-insurance.html', context=context)
