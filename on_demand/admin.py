from django.contrib import admin

from .models import Profile, Assest, InsureAssest, PaymentTransaction, Chat
from django.utils.html import mark_safe


class InsureAssestAdmin(admin.ModelAdmin):
    list_display = (
        "assest",
        "start_time",
        "end_time",
        "premium",
        "is_paid",
        "is_claim_completed",
        "split_payments",
        "insure_assest_ref",
        "chat"
    )
    readonly_fields = ()

    def chat(self, obj):
        return mark_safe('<a href="/claim/{0}">Chat</a>'.format(obj.insure_assest_ref))
    chat.short_description = "Chat"

    class Meta:
        model = InsureAssest
admin.site.register(InsureAssest, InsureAssestAdmin)

admin.site.register([Profile, Assest, PaymentTransaction, Chat])
