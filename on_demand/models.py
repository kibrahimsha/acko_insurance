import uuid

from django.contrib.auth.models import User
from django.db import models
from django_tools.middlewares import ThreadLocal


def generate_uniq_id():
    return uuid.uuid4().hex.upper()


class AuditModel(models.Model):
    """
    Audit Model
    """
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    created_by = models.ForeignKey(
        "auth.User",
        related_name="created_%(class)s_set",
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING
    )
    modified_by = models.ForeignKey(
        "auth.User",
        related_name="modified_%(class)s_set",
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING
    )
    class Meta:
        abstract = True

    def save_audit_user(self):
        """
        Save Audit User
        :return:
        """
        user = ThreadLocal.get_current_user()
        if user and user.is_authenticated():
            if not self.id:
                self.created_by = user
            self.modified_by = user

    def save(self, *args, **kwargs):
        """
         Add this line --> self.save_audit_user() <-- in every overriding save method
        :param args:
        :param kwargs:
        :return:
        """
        self.save_audit_user()
        super(AuditModel, self).save(*args, **kwargs)


class Profile(AuditModel):
    """
    Profile
    """
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)
    pan_card_no = models.CharField(max_length=30, null=True, blank=True)
    aadhaar_card_no = models.CharField(max_length=30, null=True, blank=True)
    mobile_no = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return self.user.first_name


class Assest(AuditModel):
    STATUS_CHOICES = (
        ('D', 'Draft'),
        ('P', 'Pending'),
        ('A', 'Approved'),
    )
    name = models.CharField(max_length=2056)
    invoice = models.FileField(upload_to='uploads/')
    image = models.FileField(upload_to='uploads/', null=True, blank=True)
    brand = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    purchased_at = models.DateTimeField()
    amount = models.FloatField()
    serial_no = models.CharField(max_length=2056)
    assest_ref = models.CharField(max_length=255, unique=True, default=generate_uniq_id)
    status = models.CharField(max_length=20, default='D', choices=STATUS_CHOICES)
    profile = models.ForeignKey(Profile, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name


class PaymentTransaction(AuditModel):
    assest = models.ForeignKey(Assest, on_delete=models.DO_NOTHING)
    claim_amount = models.FloatField()
    payment_ref = models.CharField(max_length=255, unique=True, default=generate_uniq_id)

    def __str__(self):
        return self.assest


class InsureAssest(AuditModel):
    assest = models.ForeignKey(Assest, on_delete=models.DO_NOTHING)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    premium = models.FloatField()
    is_paid = models.BooleanField(default=False)
    is_claim_completed = models.BooleanField(default=False)
    split_payments = models.BooleanField(default=False)
    transactions = models.ManyToManyField(PaymentTransaction)
    insure_assest_ref = models.CharField(max_length=255, unique=True, default=generate_uniq_id)

    def __str__(self):
        return str(self.insure_assest_ref)


class Chat(AuditModel):
    SUBJECT_CHOICES = (
        ('A', 'Assest'),
        ('T', 'Travel'),
    )
    sender = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='sender')
    receiver = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    message = models.TextField()
    subject = models.CharField(choices=SUBJECT_CHOICES, max_length=10)
    insured_assest = models.ForeignKey(InsureAssest, null=True, blank=True, on_delete=models.DO_NOTHING)
    insured_trip = models.ForeignKey('travel.InsureTrip', null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.message



