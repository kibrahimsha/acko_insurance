from django.apps import AppConfig


class OnDemandConfig(AppConfig):
    name = 'on_demand'
