// Slide out menu
// document.addEventListener('DOMContentLoaded', function() {
//     var elems = document.querySelectorAll('.sidenav');
//     var instances = M.Sidenav.init(elems, options);
// });

$(document).ready(function(){
    $('.sidenav').sidenav();
    $('.datepicker').datepicker();
    $('.timepicker').timepicker();
    $('.modal').modal();
});

// Django CSRF
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});



$('.insure-trigger').click(function () {
    var btn = $(this);
    var asset_id = btn.attr('data-id');
    $('#modal_asset_id').val(asset_id);
});

$("#end_time").change(function () {
    var startTime = $("#start_date").val() + " " + $("#start_time").val();
    var endTime = $("#end_date").val() + " " + $("#end_time").val();
    var assestId = $("#modal_asset_id").val()
    var postData = {'startTime': startTime, 'endTime': endTime, 'assestId': assestId};
    $.ajax({
        type: "POST",
        url: "/get-premiums",
        data: postData,
        statusCode: {
            200: function(data) {
                var response = JSON.parse(data);
                for (var i=0; i<=response.length-1; i++){
                    var htmlContent = '<div class="input-field col s12">' +
                    '<div>' +
                        '<label>'+
                            '<input name="premium" type="radio" class="premium_amount_options" data-val="' + response[i]["amount"] + '"/>' +
                            '<span class="premium-radio--txt">Start Time: </span><span class="premium-radio--style--cont">' + response[i]["startTime"] +'  /  </span>'+
                            '<span class="premium-radio--txt">End Time: </span><span class="premium-radio--style--cont">' + response[i]["endTime"]+'  /  </span>' +
                            '<span class="premium-radio--txt">Hours: </span><span class="premium-radio--style--cont">' + response[i]["hours"]+' Hrs  /  </span>' +
                            '<span class="premium-radio--txt">Amount: </span><span class="premium-radio--style--cont">Rs.' + response[i]["amount"]+'</span>' +
                        '</label>'+
                    '</div>'+
                '</div>';
                $("#hours_respone").append(htmlContent);
                }

                console.log(response)
            },
            400: function(response) {
            },
            500: function(response){
            }
        },
    });
});

$("#insure_asset_submit").click(function(){
    var startTime = $("#start_date").val() + " " + $("#start_time").val();
    var endTime = $("#end_date").val() + " " + $("#end_time").val();
    var assestId = $("#modal_asset_id").val();
    var premium = $("#premium").val();

    var postData = {
        'startTime': startTime,
        'endTime': endTime,
        'assestId': assestId,
        'premium': premium
    };
        $.ajax({
        type: "POST",
        url: "/create-insure-asset",
        data: postData,
        statusCode: {
            200: function(data) {
                location.reload()
            },
            400: function(response) {
            },
            500: function(response){
            }
        },
    });


});

$(document).on("click", ".premium_amount_options", function() {
    var btn = $(this);
    var premium = btn.attr('data-val');
    $('#premium').val(premium);
});

$("#sendMsg").click(function() {
    var insured_assest_id = $('#insured_assest_id').val();
    var message = $('#message').val();
    var receiver_id = $('#receiver_id').val();

    var postData = {
        'insured_assest_id': insured_assest_id,
        'receiver_id': receiver_id,
        'message': message
    };
    $.ajax({
        type: "POST",
        url: "/create-chat",
        data: postData,
        statusCode: {
            200: function(data) {
                location.reload()
            },
            400: function(response) {
            },
            500: function(response){
            }
        },
    });


});

