"""acko_insurance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from on_demand.views import user_login, user_logout, user_register, user_profile, assest_list, create_assest, \
    create_insure_assest, get_premiums, assest_detail, chat_history, insure_assest_list, create_chat, \
    claim_insurance

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', user_login, name='user_login'),
    path('register/', user_register, name='user_register'),
    path('logout/', user_logout, name='user_logout'),
    path('profile/', user_profile, name='user_profile'),
    path('add-asset/', create_assest, name='create_assest'),
    path('asset-list/', assest_list, name='assest_list'),
    path('asset-detail/<assest_ref>/', assest_detail, name='assest_detail'),
    path('get-premiums', get_premiums, name='get_premiums'),
    path('create-insure-asset', create_insure_assest, name='create_insure_assest'),
    path('insure-asset-list', insure_assest_list, name='insure_assest_list'),
    path('chat-history', chat_history, name='chat_history'),
    path('create-chat', create_chat, name='create_chat'),
    path('claim/<assest_ref>', claim_insurance, name='claim_insurance'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
